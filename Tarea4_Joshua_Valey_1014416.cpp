#include "pch.h"
#include <iostream>

using namespace System;
using namespace System::Threading;
//using namespace std;

//Variables Globales
char pista[72]; //El array es 72 por la pos 0 en la carreara y por el caracter final null por defecto en los vectores de tipo char en c++; 
char* dir_pista;
int posTortuga = 1;
int posConejo = 1;
/*    *///Declaracion del reloj
/*   */int reloj = 0;

//Declaracion de metodos

//Metodos de impresion. 
void ImprimirReloj();
void ImprimirPista();
void ImprimirUi();
//Metodos de Cambio de pos
void NuevaPos(int&, int&, int, int);
void AvanceTortuga();
void AvanceConejo();
//Metodos Carrera Finalizada. 
bool CarreraFinalizada();
void TortugaGana();
void ConejoGana();
void Empate();
//Util
int NumeroRandom(Random^);


int main(array<System::String^>^ args)
{
	dir_pista = pista;

	//Inicializar array. 
	//Es hasta 71 y no 72 por el valor final por defecto de un array char en c++
	for (int i = 0; i < 71; i++)
	{
		if (i == 0)
			pista[i] = ' ';
		else
			pista[i] = '-';
	}

	Console::WriteLine("�BANG! �Y ARRANCAN!");
	while (!CarreraFinalizada())
	{
		ImprimirUi();
		Thread::Sleep(1000);
		reloj++;
		Console::Clear();
	}
	return 0;
}

//1. Impresiones

void ImprimirReloj()
{
	Console::WriteLine("Segundos " + reloj.ToString() + "\n\n\n");
}
void ImprimirPista()
{
	Console::Write("Start ");
	for (int i = 0; i < 71; i++)
	{
		Console::Write(Convert::ToChar(pista[i]));

	}
	Console::Write(" Goal ");
}
void ImprimirUi()
{

	ImprimirReloj();
	ImprimirPista();
	AvanceConejo();
	AvanceTortuga();
}

//2. Casos de uso especificos. 

//Mandar en los parametros addPosition el numero de cuadros que se mueven
//los animales, a la izquierda son negativos. 
void NuevaPos(int& posTortuga, int& posConejo,
			  int addToPositionTortuga, int addToPositionConejo)
{

	/*valorEnPosTortuga = '-';
	valorEnPosConejo = '-';*/

	pista[posTortuga] = '-';
	pista[posConejo] = '-';

	//Si la pos de algun animal es menor que uno este debe regresar a la pos 1
	if ((posTortuga + addToPositionTortuga) < 1)
	{
		posTortuga = 1;
	}

	if ((posConejo + addToPositionConejo) < 1)
	{
		posConejo = 1;
	}

	//Si la pos de algun animal es mayor que 71 este debe regresar a la pos 71
	if ((posTortuga + addToPositionTortuga) > 71)
	{
		posTortuga = 71;
	}

	if ((posConejo + addToPositionConejo) > 71)
	{
		posConejo = 71;
	}


	//Si la posicion no se pasa dar los nuevos valores. 
	if (!(posTortuga + addToPositionTortuga < 1) && !(posTortuga + addToPositionTortuga > 71))
	{
		posTortuga += addToPositionTortuga;
	}
	if (!(posConejo + addToPositionConejo < 1) && !(posConejo + addToPositionConejo > 71))
	{
		posConejo += addToPositionConejo;
	}

	//Mandar nuevos valores al array
	if (posTortuga == posConejo)
	{
		pista[posTortuga] = 'O'; //O de OUCH!! 
	}
	else
	{
		pista[posTortuga] = 'T';
		pista[posConejo] = 'H';
	}



	//posActualTortuga = '-';
	//posActualConejo = '-';
	//nuevaPosTortuga = 'T';
	//nuevaPosConejo = 'H';
}
void AvanceTortuga()
{
	int numeroRdn = NumeroRandom(gcnew Random());

	if (numeroRdn >= 1 && numeroRdn <= 5)
	{
		//Paso Veloz
		NuevaPos(posTortuga, posConejo, 3, 0);
	}
	else if (numeroRdn >= 6 && numeroRdn <= 7)
	{
		//Resbalon
		NuevaPos(posTortuga, posConejo, -6, 0);
	}
	else if (numeroRdn >= 8 && numeroRdn <= 10)
	{
		//Paso Lento
		NuevaPos(posTortuga, posConejo, 1, 0);
	}
}
void AvanceConejo()
{
	int numeroRdn = NumeroRandom(gcnew Random());

	if (numeroRdn >= 1 && numeroRdn <= 2)
	{
		//Duerme
		NuevaPos(posTortuga, posConejo, 0, 0);
	}
	else if (numeroRdn >= 3 && numeroRdn <= 4)
	{
		//Gran Salto
		NuevaPos(posTortuga, posConejo, 0, 9);
	}
	else if (numeroRdn >= 5 && numeroRdn <= 6)
	{
		//GranResbalon
		NuevaPos(posTortuga, posConejo, 0, -12);
	}
	else if (numeroRdn >= 7 && numeroRdn <= 8)
	{
		//Salto Pequenio
		NuevaPos(posTortuga, posConejo, 0, 1);

	}
	else if (numeroRdn >= 9 && numeroRdn <= 10)
	{
		//Resbalon Pequenio. 
		NuevaPos(posTortuga, posConejo, 0, -2);

	}
}
bool CarreraFinalizada()
{
	//Empate.
	if ((posConejo >= 71) && (posTortuga >= 71))
	{
		Empate();
		return true;
	}
	//GanadorEspecifico. 
	else if ((posConejo >= 71) || (posTortuga >= 71))
	{

		(posTortuga >= 71) ?
			TortugaGana() : ConejoGana();
		return true;
	}
	//ChecarEsto. 
	return false;
}
void TortugaGana()
{
	Console::WriteLine("�LA TORTUGA GANA! �BRAVO!");
}
void ConejoGana()
{
	Console::WriteLine("�La liebre gana, Ni hablar!");

}
void Empate()
{
	Console::WriteLine("Es un Empate!!! La Tortuga logro EMPATAR!!!");
}

//3. Utilidades
int NumeroRandom(Random^ random)
{
	//Random^ random = gcnew Random();
	return random->Next(1, 11);
}